﻿using MovieTrailers.Core.DTOs;
using Xunit;

namespace MovieTrailers.UnitTests.Core.Entities
{
    public class VideoTests
    {
        [Fact]
        public void VideoEntity_Generates_ACorrectYoutubeURL() 
        {
            //arrange
            var key = "zAGVQLHvwOY";
            var video = new VideoDTO()
            {
                Key = key,
                Site = "youtube"
            };

            //act
            var result = video.VideoUrl;

            //assert
            Assert.Equal(result, $"youtube.com/watch?v={key}");
        }


        [Fact]
        public void VideoEntity_Generates_A_informationalURL_forUnknownTrailerProvider()
        {
            //arrange
            var key = "zAGVQLHvwOY";
            var site = "unknown";
            var video = new VideoDTO()
            {
                Key = key,
                Site = site
            };

            //act
            var result = video.VideoUrl;

            //assert
            Assert.Equal(result, $"Site:{site},Key:{key}");
        }
    }
}
