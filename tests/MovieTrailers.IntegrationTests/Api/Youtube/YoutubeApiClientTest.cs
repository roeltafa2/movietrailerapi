﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using MovieTrailers.Core.Common.Mappings;
using MovieTrailers.Infrastructure.Api.Youtube;
using Xunit;

namespace MovieTrailers.IntegrationTests.Api.Youtube
{
    public class YoutubeApiClientTest
    {
        private readonly Mock<IConfiguration> _configuration;
        private readonly IMapper _mapper;
        public YoutubeApiClientTest()
        {
            _configuration = new Mock<IConfiguration>();
            _configuration.SetupGet(x => x[It.Is<string>(s => s == "Youtube:ApiKey")]).Returns("AIzaSyCupApvi128gXoCMSpS2y60Pe6pgajH35w");
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            _mapper = mockMapper.CreateMapper();

        }

        [Fact]
        public void Search_Videos()
        {
            var client = new YoutubeApiClient(_configuration.Object, _mapper);

            //act
            var result = client.Videos("joker").Result;

            //assert
            Assert.True(result.Count > 0);
        }
    }
}
