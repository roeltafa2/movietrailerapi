﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using MovieTrailers.Core.Common.Mappings;
using MovieTrailers.Infrastructure.Api;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MovieTrailers.IntegrationTests.Api.TheMovieDB
{
    public class MoviesApiClientTests
    {
        private readonly Mock<IConfiguration> _configuration;
        private readonly IMapper _mapper;
        public MoviesApiClientTests()
        {
            _configuration = new Mock<IConfiguration>();
            _configuration.SetupGet(x => x[It.Is<string>(s => s == "TheMovieDB:ApiKey")]).Returns("2bdd3c7cd3b1a0d237f5986e5418e4eb");
            _configuration.SetupGet(x => x[It.Is<string>(s => s == "TheMovieDB:BaseUri")]).Returns("https://api.themoviedb.org/3/");
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Fact]
        public void Return_Genres()
        {                      
            var client = new MoviesApiClient(_configuration.Object, _mapper);

            //act
            var result = client.Genres().Result;

            //assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void Return_Keywords()
        {
            var client = new MoviesApiClient(_configuration.Object, _mapper);

            var result = client.Keywords("ultra",1).Result;

            //assert
            Assert.True(result.Count > 0);
        }

        [Fact]
        public void Search_Movies()
        {
            var client = new MoviesApiClient(_configuration.Object, _mapper);

            var result = client.Search("joker", 1).Result;

            //assert
            Assert.True(result.Count > 0);
        }


    }
}
