﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MovieTrailers.Core.Interfaces;
using MovieTrailers.Core.DTOs;

namespace MovieTrailers.Web.Api
{
    public class MoviesController : BaseApiController
    {
        private IMoviesApiClient _apiClient;
        private IYoutubeApiClient _youtubeClient;
        public MoviesController(IMoviesApiClient apiClient,IYoutubeApiClient youtubeClient, IPolicies policies)
            : base(policies)
        {
            _apiClient = apiClient;
            _youtubeClient = youtubeClient;
        }

        [HttpGet("~/search")]
        public IActionResult Search(string searchTerm, int page = 1)
        {
            if (page <= 0)
                throw new ArgumentException("Page should be a positive number and biger then 0!");
            
            var items =
                getResult(context => _apiClient.Search(searchTerm, page).Result, $"{searchTerm}-{page}");

            return Ok(items);
        }

        [HttpGet("~/videos")]
        public IActionResult Videos(int movieId)
        {
            var items =
                getResult(context => _apiClient.Videos(movieId).Result, movieId.ToString());
            return Ok(items);
        }

        [HttpGet("~/youtube-videos")]
        public IActionResult Videos(string title)
        {
            var items =
                getResult(context => _youtubeClient.Videos(title).Result, title);
            return Ok(items);
        }    
    }
}