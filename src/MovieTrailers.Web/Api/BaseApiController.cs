﻿using Microsoft.AspNetCore.Mvc;
using MovieTrailers.Core.Interfaces;
using Polly;
using System;

namespace MovieTrailers.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseApiController : Controller
    {
        protected IPolicies _policies;

        public BaseApiController(IPolicies policies)
        {
            _policies = policies;
        }

        protected T getResult<T>(Func<Context, T> func, string cacheKey)
            where T : class => _policies.CachePolicy
                .Execute<T>(func, new Context(cacheKey));

    }
}
