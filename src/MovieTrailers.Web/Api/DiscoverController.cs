﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MovieTrailers.Core.DTOs;
using MovieTrailers.Core.Interfaces;

namespace MovieTrailers.Web.Api
{
    public class DiscoverController : BaseApiController
    {
        private IMoviesApiClient _apiClient;
        public DiscoverController(IMoviesApiClient apiClient, IPolicies policies)
            : base(policies)
        {
            _apiClient = apiClient;
        }

        [HttpGet("~/discover")]
        public  IActionResult Discover(int year, string genres, string keywords, int page = 1)
        {
            if (page <= 0)
                throw new ArgumentException("Page should be a positive number and biger then 0!");

            var searchArgument = new DiscoverMoviesDTO(year, ToIntList(genres), ToIntList(keywords));
            var items =
                getResult(context => _apiClient.Discover(searchArgument, page).Result, $"{searchArgument.ToString()}-{page}");

            return Ok(items);
        }

        [HttpGet("~/genres")]
        public IActionResult Genres()
        {
            var result =
                getResult(context => _apiClient.Genres().Result, "GetGenres");

            return Ok(result);
        }

        [HttpGet("~/keywords")]
        public IActionResult Keywords(string searchTerm,int page = 1)
        {
            if (page <= 0) page = 1;

            var result =
                getResult(context => _apiClient.Keywords(searchTerm,page).Result, $"Keywords:{searchTerm}-{page}");

            return Ok(result);
        }

        private List<int> ToIntList(string input) => string.IsNullOrWhiteSpace(input) ? new List<int>()
           : input.Split(',')
           .Select(x => int.Parse(x))
           .ToList();

    }
}