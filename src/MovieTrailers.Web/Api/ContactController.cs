﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieTrailers.Core.DTOs;
using MovieTrailers.Core.Interfaces;

namespace MovieTrailers.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private IContactService _contactService;
        public ContactController(IContactService contactService) 
        {
           _contactService = contactService;
        }

        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ContactDTO contact)
        {
            await _contactService.SendMessage(contact);
            return Ok(true);
        }

    }
}
