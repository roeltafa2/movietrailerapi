﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using MovieTrailers.Core.ApiResponses;
using MovieTrailers.Core.DTOs;
using MovieTrailers.Core.Interfaces;

namespace MovieTrailers.Infrastructure.Api
{
    /// <summary>
    /// Documentation regarding the TMDB API: https://developers.themoviedb.org/3/getting-started
    /// </summary>
    public class MoviesApiClient : IMoviesApiClient
    {
        private string _apiKey;
        private string _apiKeyConfigName = "TheMovieDB:ApiKey";
        private string _baseUriConfigName = "TheMovieDB:BaseUri";

        private readonly string BaseApiEndpoint;
        private HttpClient _client = new HttpClient();
        private readonly IMapper _mapper;

        public MoviesApiClient(IConfiguration config, IMapper mapper)
        {
            _apiKey = config[_apiKeyConfigName];
            BaseApiEndpoint = config[_baseUriConfigName];
            _mapper = mapper;
        }

        public async Task<List<MovieDTO>> Search(string searchTerm, int page)
        {
            var uri = GetSearchUri(searchTerm, page);
            
            var apiMovies = await CallApi<MovieSearchResult>(uri);

            return _mapper.Map<List<MovieDTO>>(apiMovies.results);
        }

        public async Task<List<MovieDTO>> Discover(DiscoverMoviesDTO searchArguments,int page)
        {
            var uri = GetDiscoverUri(searchArguments, page);

            var apiMovies = await CallApi<MovieSearchResult>(uri);

            return _mapper.Map<List<MovieDTO>>(apiMovies.results);
        }

        public async Task<List<GenreDTO>> Genres()
        {
            var uri = GetGenresUri();

            var genresResult = await CallApi<GenresResult>(uri);

            return _mapper.Map<List<GenreDTO>>(genresResult.genres);
        }

        public async Task<List<KeywordDTO>> Keywords(string searchTerm, int page) 
        {
            var uri = GetKeywordsUri(searchTerm, page);

            var result = await CallApi<KeywordResult>(uri);

            return _mapper.Map<List<KeywordDTO>>(result.Results);
        }



        public async Task<List<VideoDTO>> Videos(int movieId)
        {
            var uri = GetVideosUri(movieId);

            var videos = await CallApi<TmdbVideoResult>(uri);

            return _mapper.Map<List<VideoDTO>>(videos.Results);
        }

        private async Task<T> CallApi<T>(string uri) where T : class
        {
            var result = await _client.GetAsync(uri);

            result.EnsureSuccessStatusCode();
            
            return await result.Content.ReadAsAsync<T>();
        }

        private string GetVideosUri(int movieId) =>  $"{BaseApiEndpoint}" +
            $"movie/{movieId}/videos" +
            $"?api_key={_apiKey}&language=en-US";

        private string GetSearchUri(string searchTerm, int page) => $"{BaseApiEndpoint}" +
            $"search/movie?api_key={_apiKey}" +
            $"&language=en-US&query={HttpUtility.UrlEncode(searchTerm)}" +
            $"&page={page}&include_adult=false";

        private string GetDiscoverUri(DiscoverMoviesDTO args, int page) => $"{BaseApiEndpoint}" +
            $"discover/movie?api_key={_apiKey}" +
            $"&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false" +
            $"&page={page}" +
            (args.Year == 0 ? "" 
                    : $"&year={args.Year}") +
            (args.Genres.Count == 0 ? "" 
                    : $"&with_genres={HttpUtility.UrlEncode(string.Join(',',args.Genres))}") +
            (args.Keywords.Count == 0 ? "" 
                    : $"&with_keywords={HttpUtility.UrlEncode(string.Join(',', args.Keywords))}");

        private string GetGenresUri() => $"{BaseApiEndpoint}" +
            $"genre/movie/list?api_key={_apiKey}";

        private string GetKeywordsUri(string searchTerm, int page) => $"{BaseApiEndpoint}" +
            $"search/keyword?api_key={_apiKey}" +
            $"&page={page}" +
            $"&query={HttpUtility.UrlEncode(searchTerm)}";
    }
}