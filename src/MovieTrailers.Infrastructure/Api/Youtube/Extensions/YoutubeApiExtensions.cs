﻿using Google.Apis.YouTube.v3.Data;
using MovieTrailers.Core.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace MovieTrailers.Infrastructure.Api.Youtube.Extensions
{
    public static class YoutubeApiExtensions
    {
        public static List<VideoDTO> toVideos(this IList<SearchResult> items) =>
            items.Select(x => new VideoDTO()
            {
                Id = 0,
                Uid = x.Id.VideoId,
                Key = x.Id.VideoId,
                Name = x.Snippet.Title,
                Site = "Youtube",
                Type = "Trailer",
            }).ToList();
    }
}
