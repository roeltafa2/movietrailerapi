﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MovieTrailers.Core.Interfaces;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using MovieTrailers.Infrastructure.Api.Youtube.Extensions;
using MovieTrailers.Core.DTOs;
using AutoMapper;

namespace MovieTrailers.Infrastructure.Api.Youtube
{
    public class YoutubeApiClient : IYoutubeApiClient
    {
        private string _apiKey;
        private string _apiKeyConfigName = "Youtube:ApiKey";
        private YouTubeService _youtubeService;
        private int _maxResult = 50;
        private readonly IMapper _mapper;
        public YoutubeApiClient(IConfiguration config, IMapper mapper)
        {
            _apiKey = config[_apiKeyConfigName];
            _youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = _apiKey,
                ApplicationName = this.GetType().ToString()
            });
            _mapper = mapper;
        }


        public async Task<List<VideoDTO>> Videos(string title)
        {
            var searchListRequest = getSearchVideosRequest(title);

            var searchListResponse = await searchListRequest.ExecuteAsync();

            return searchListResponse.Items.toVideos();
        }

        private SearchResource.ListRequest getSearchVideosRequest(string title)
        {
            var searchListRequest = _youtubeService.Search.List("snippet");

            searchListRequest.Type = "video";
            searchListRequest.Q = $"{title}";
            searchListRequest.MaxResults = _maxResult;
            return searchListRequest;
        }
    }
}