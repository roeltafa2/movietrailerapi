﻿using Microsoft.Extensions.Configuration;
using MovieTrailers.Core.DTOs;
using MovieTrailers.Core.Interfaces;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MovieTrailers.Infrastructure.Mail
{
    public class ContactService : IContactService
    {
        private string _mailAddressConfig = "Mail:Address";
        private string _passwordConfig = "Mail:Password";
        private string _smtpConfig = "Mail:Smtp";

        private string _mailAddress;
        private string _smtp;

        private NetworkCredential _credentials;

        public ContactService(IConfiguration config) 
        {
            _mailAddress = config[_mailAddressConfig];
            _smtp = config[_smtpConfig];
            _credentials = new NetworkCredential(config[_mailAddressConfig], config[_passwordConfig]);
        }


        public async Task SendMessage(ContactDTO contact)
        {
            try
            {
                MailMessage mail = getMail(contact);
                SmtpClient client = getClient();

                await client.SendMailAsync(mail);
            }
            catch (Exception)
            {
                throw;
            }         
        }

        private SmtpClient getClient()
        {
            var client = new SmtpClient()
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = _smtp,
                EnableSsl = true
            };
            client.Credentials = _credentials;

            return client;
        }

        private MailMessage getMail(ContactDTO contact)
        {
            var mail = new MailMessage(contact.EmailAddress, _mailAddress)
            {
                Subject = $"Message From : {contact.FullName}",
                Body = contact.Message
            };
            mail.IsBodyHtml = true;
            return mail;
        }
    }
}
