﻿using Microsoft.Extensions.Caching.Memory;
using MovieTrailers.Core.Interfaces;
using Polly;
using Polly.Caching;
using Polly.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Infrastructure.Policies
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/architecture/microservices/implement-resilient-applications/implement-http-call-retries-exponential-backoff-polly
    /// </summary>
    public class PollyPolicies : IPolicies
    {
        private int expireInMinutes = 10;
        public CachePolicy CachePolicy { get; }

        public PollyPolicies()
        {
            var memoryCache = new MemoryCache(new MemoryCacheOptions());
            var memoryCacheProvider = new MemoryCacheProvider(memoryCache);

            CachePolicy = Policy.Cache(memoryCacheProvider, TimeSpan.FromMinutes(expireInMinutes));
        }
    }
}
