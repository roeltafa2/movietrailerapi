﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using MovieTrailers.Infrastructure.Api;
using MovieTrailers.Infrastructure.Policies;
using MovieTrailers.Core.Interfaces;
using MovieTrailers.Core.ApiResponses;
using MovieTrailers.Core.Common.Mappings;
using AutoMapper;
using System.Linq;

namespace MovieTrailers.Infrastructure
{
    public static class ContainerSetup
    {
        public static IServiceProvider InitializeWeb(Assembly webAssembly, IServiceCollection services) =>
            new AutofacServiceProvider(BaseAutofacInitialization(setupAction =>
            {
                setupAction.Populate(services);
                setupAction.RegisterAssemblyTypes(webAssembly).AsImplementedInterfaces();
            }));

        public static IContainer BaseAutofacInitialization(Action<ContainerBuilder> setupAction = null)
        {
            var builder = new ContainerBuilder();

            var coreAssembly = Assembly.GetAssembly(typeof(Movie));
            var infrastructureAssembly = Assembly.GetAssembly(typeof(MoviesApiClient));
            builder.RegisterAssemblyTypes(coreAssembly, infrastructureAssembly).AsImplementedInterfaces();
            builder.RegisterInstance<IPolicies>(new PollyPolicies());

            builder.Register(ctx => new MapperConfiguration(cfg =>
            {             
                    cfg.AddProfile(new MappingProfile());
            }));

            builder.Register(ctx => ctx.ResolveOptional<MapperConfiguration>().CreateMapper()).As<IMapper>();

            setupAction?.Invoke(builder);
            return builder.Build();
        }
    }
}
