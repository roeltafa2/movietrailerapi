﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.DTOs
{
    public class KeywordDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
