﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.DTOs
{
    public class VideoDTO
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Site { get; set; }
        public string Type { get; set; }
        public string VideoUrl
        {
            get => getVideoUrl();
        }

        private string getVideoUrl() => Site.ToLower() switch
        {
            "youtube" => $"youtube.com/watch?v={Key}",
            _ => $"Site:{Site},Key:{Key}"
        };
    }
}
