﻿using System.ComponentModel.DataAnnotations;

namespace MovieTrailers.Core.DTOs
{
    public class ContactDTO
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
        [Required]
        public string Message { get; set; }
    }
}