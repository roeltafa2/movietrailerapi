﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.DTOs
{
    public class DiscoverMoviesDTO
    {
        public DiscoverMoviesDTO(int year, List<int> genres, List<int> keywords)
        {
            Year = year;
            Genres = genres;
            Keywords = keywords;
        }

        public int Year { get; set; }
        public List<int> Genres { get; set; } = new List<int>();
        public List<int> Keywords { get; set; } = new List<int>();

        /// <summary>
        /// This is needed for the cache key.
        /// </summary>
        public override string ToString()
        {
            return $"Year:{Year.ToString()}," +
                $"Genres:[{string.Join(',', Genres)}]," +
                $"KeyWords:[{string.Join(',', Keywords)}]";
        }
    }
}
