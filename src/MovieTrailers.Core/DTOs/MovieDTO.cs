﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.DTOs
{
    public class MovieDTO
    {
        public int Id { get; set; }
        public string PosterPath { get; set; }
        public string OriginalTitle { get; set; }
        public string Title { get; set; }
        public bool Video { get; set; }
    }
}
