﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.ApiResponses
{
    /// <summary>
    /// Class for videos from youtube
    /// </summary>
    public class Video
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Site { get; set; }
        public string Type { get; set; }      
    }
}
