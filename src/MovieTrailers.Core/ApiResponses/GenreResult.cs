﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.ApiResponses
{
    public class GenresResult
    {
        public List<Genre> genres { get; set; }
    }
}
