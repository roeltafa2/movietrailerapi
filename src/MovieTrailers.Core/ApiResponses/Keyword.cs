﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.ApiResponses
{
    public class Keyword
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
