﻿using System.Collections.Generic;

namespace MovieTrailers.Core.ApiResponses
{
    public class MovieSearchResult
    {
        public int page { get; set; }
        public List<Movie> results { get; set; }
        public int total_results { get; set; }
        public int total_pages { get; set; }
    }
}
