﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.ApiResponses
{
    public class TmdbVideoResult
    {
        public int Id { get; set; }
        public List<TmdbVideo> Results { get; set; }
    }
}
