﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieTrailers.Core.ApiResponses
{

    public class KeywordResult
    {
        public int Page { get; set; }
        public List<Keyword> Results { get; set; }
        public int Total_pages { get; set; }
        public int Total_results { get; set; }
    }
}
