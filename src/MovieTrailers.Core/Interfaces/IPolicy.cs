﻿using Polly.Caching;

namespace MovieTrailers.Core.Interfaces
{
    public interface IPolicies
    {
       CachePolicy CachePolicy
       {
            get;
       }
    }
}
