﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieTrailers.Core.DTOs;

namespace MovieTrailers.Core.Interfaces
{
    public interface IMoviesApiClient
    {
        Task<List<MovieDTO>> Search(string searchTerm,int page);
        Task<List<MovieDTO>> Discover(DiscoverMoviesDTO searchArguments, int page);
        Task<List<VideoDTO>> Videos(int movieId);
        Task<List<GenreDTO>> Genres();
        Task<List<KeywordDTO>> Keywords(string searchTerm, int page);


    }
}
