﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieTrailers.Core.DTOs;

namespace MovieTrailers.Core.Interfaces
{
    public interface IYoutubeApiClient
    {
        Task<List<VideoDTO>> Videos(string title);
    }
}
