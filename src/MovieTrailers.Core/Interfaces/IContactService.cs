﻿using MovieTrailers.Core.DTOs;
using System.Threading.Tasks;

namespace MovieTrailers.Core.Interfaces
{
    public interface IContactService
    {
        Task SendMessage(ContactDTO contact);
    }
}
