﻿using AutoMapper;
using MovieTrailers.Core.ApiResponses;
using MovieTrailers.Core.DTOs;

namespace MovieTrailers.Core.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Genre, GenreDTO>();
            CreateMap<Movie, MovieDTO>().ForMember(dest => dest.PosterPath, opts => opts.MapFrom(src => src.poster_path))
                                        .ForMember(dest => dest.OriginalTitle, opts => opts.MapFrom(src => src.original_title));
            CreateMap<Keyword, KeywordDTO>();            
        }       
    }
}
